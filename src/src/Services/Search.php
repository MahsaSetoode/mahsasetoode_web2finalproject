<?php

namespace App\Services;

use App\Entity\Task;
use App\Repository\TaskRepository;
use Doctrine\ORM\EntityManagerInterface;

class Search
{
    /**
     * @var \Doctrine\ORM\EntityManagerInterface
     */
    private EntityManagerInterface $entityManager;

    public function __construct(EntityManagerInterface $entityManager)
    {
        $this->entityManager = $entityManager;
    }

    /**
     * @param $nameQuery
     * @return Task[]
     */
    public function searchTask($nameQuery, $id):array{
        /**
         * @var TaskRepository $taskRepository
         */
        $taskRepository = $this->entityManager->getRepository(Task::class);
        return $taskRepository->searchByTitle($nameQuery, $id);
    }

    /**
     * @param $nameQuery
     * @return Task[]
     */
    public function searchTaskAdmin($nameQuery):array{
        /**
         * @var TaskRepository $taskRepository
         */
        $taskRepository = $this->entityManager->getRepository(Task::class);
        return $taskRepository->searchByTitleAdmin($nameQuery);
    }

}