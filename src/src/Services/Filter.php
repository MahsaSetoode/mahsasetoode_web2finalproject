<?php

namespace App\Services;

use App\Entity\Task;
use App\Repository\TaskRepository;
use Doctrine\ORM\EntityManagerInterface;

class Filter
{
    /**
     * @var EntityManagerInterface
     */
    private EntityManagerInterface $entityManager;

    public function __construct(EntityManagerInterface $entityManager)
    {
        $this->entityManager = $entityManager;
    }

    /**
     * @param $state
     * @return Task[]
     */
    public function filterTask($state, $companyId):array{
        $task_state = false;
        if ($state == "completed"){
            $task_state = true;
        }
        /**
         * @var TaskRepository $taskRepository
         */
        $taskRepository = $this->entityManager->getRepository(Task::class);
        return $taskRepository->findBy(['status' => $task_state,
            'company' => $companyId]);
    }

    public function filterTaskAdmin($state):array{
        $task_state = false;
        if ($state == "completed"){
            $task_state = true;
        }
        /**
         * @var TaskRepository $taskRepository
         */
        $taskRepository = $this->entityManager->getRepository(Task::class);
        return $taskRepository->findBy(['status' => $task_state]);
    }

}