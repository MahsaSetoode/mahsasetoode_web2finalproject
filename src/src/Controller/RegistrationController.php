<?php

namespace App\Controller;

use App\Entity\User;
use App\Form\RegistrationFormType;
use App\Security\AppAuthenticator;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\PasswordHasher\Hasher\UserPasswordHasherInterface;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Core\Security;
use Symfony\Contracts\Translation\TranslatorInterface;

class RegistrationController extends AbstractController
{
//    #[Route('/{_locale}/signup', name: 'app_register', requirements: [
//        '_locale' => 'en|fa',
//    ],
//        defaults: ["_locale"=>"en"])]
    #[Route('/signup', name: 'app_register')]
    public function register(Request $request, UserPasswordHasherInterface $userPasswordHasher,
                             EntityManagerInterface $entityManager, Security $security, AppAuthenticator $appAuthenticator): Response
    {
        $user = new User();
        $form = $this->createForm(RegistrationFormType::class, $user);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            // encode the plain password
            $user->setPassword(
                $userPasswordHasher->hashPassword(
                    $user,
                    $form->get('plainPassword')->getData()
                )
            );

            $this->addFlash('success', 'You are registered and logged in.');
            $user->setRoles(["ROLE_USER"]);
            $entityManager->persist($user);
            $entityManager->flush();
//            $token = new UsernamePasswordToken($user, null, 'ROLE_USER', $user->getRoles());
//            $security->setToken($token);
            // do anything else you need here, like send an email

//            return $guardHandler->authenticateUserAndHandleSuccess(
//                $user,          // the User object you just created
//                $request,
//                $appAuthenticator, // authenticator whose onAuthenticationSuccess you want to use
//                'main'          // the name of your firewall in security.yaml
//            );

            return $this->redirectToRoute('app_login');
        }

        return $this->render('registration/register.html.twig', [
            'registrationForm' => $form->createView(),
        ]);
    }
}
