<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Security\Http\Authentication\AuthenticationUtils;


class HomeController extends AbstractController
{
    #[Route('/{_locale}', name: 'app_home', requirements: [
        '_locale' => 'en|fa',
    ],
        defaults: ["_locale"=>"en"])]
    public function index(Request $request): Response
    {
        $currentRoute = $request->attributes->get('_route');
        // get user
        $user = $this->getUser();
        $isLoggedIn = ($user !== null);

        return $this->render('home/index.html.twig',[
            'user' => $user,
            'isLoggedIn' => $isLoggedIn,
        ]);
    }
}
