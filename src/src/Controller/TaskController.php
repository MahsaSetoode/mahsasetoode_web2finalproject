<?php

namespace App\Controller;

use App\Entity\Task;
use App\Entity\User;
use App\Form\TaskType;
use App\Repository\TaskRepository;
use App\Services\Filter;
use App\Services\Search;
use Doctrine\ORM\EntityManagerInterface;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

//#[Route('/{_locale}/task', requirements: ['_locale' => 'en|fa'],defaults: ["_locale"=>"en"])]
#[Route('/task')]
class TaskController extends AbstractController
{
    #[Route('/', name: 'app_task_index', methods: ['GET'])]
    public function index(TaskRepository $taskRepository): Response
    {
        return $this->render('task/index.html.twig', [
            'tasks' => $taskRepository->findAll(),
        ]);
    }

    #[Route('/change/{id}', name: 'change_state')]
    #[IsGranted("EDIT", "task")]
    public function change_state(Task $task, TaskRepository $taskRepository, EntityManagerInterface $entityManager): Response
    {
        $newStatus = !$task->isStatus(); // Toggle the status

        $task->setStatus($newStatus);

        // Update the task in the database
//        $taskRepository->($task, $newStatus);
        $entityManager->persist($task);
        $entityManager->flush();
        return $this->redirectToRoute('all_tasks', ['status' => "All"], Response::HTTP_SEE_OTHER);
    }

    #[Route("/all", name:"all_tasks")]
    public function listTasks(TaskRepository $taskRepository): Response
    {
        /***
         * @var User $loggedInManager
         */
        $loggedInManager = $this->getUser();
        if ($loggedInManager->getRoles()[0] == "ROLE_ADMIN"){
            $tasks = $taskRepository->findAll(); // Fetch tasks from the repository
        }else{
            $tasks = $taskRepository->findBy(['company' => $loggedInManager->getCompany()]);
        }
        if (empty($tasks)) {
            return $this->render('task/not_found.html.twig');
        }
        return $this->render('task/list.html.twig', [
            'status' => "All",
            'tasks' => $tasks,
            'username' => $loggedInManager->getUsername(),
        ]);
    }

    #[Route("/my_tasks", name:"my_tasks")]
    public function MyTasks(Request $request, TaskRepository $taskRepository): Response
    {
        $query = $request->query->get('status');
        $loggedInManager = $this->getUser();
        /***
         * @var User $loggedInManager
         */
        $tasks = $taskRepository->findBy(['assignedTo' => $loggedInManager->getId()]);
        if (empty($tasks)) {
            return $this->render('task/not_found.html.twig');
        }
        return $this->render('task/list.html.twig', [
            'status' => $query,
            'tasks' => $tasks,
            'username' => $loggedInManager->getUsername(),
        ]);
    }

    #[Route("/search", name:"search_task")]
    public function searchTask(Request $request, Search $search): Response
    {
        $query = $request->query->get('t');
        $loggedInManager = $this->getUser();
        if($loggedInManager->getRoles()[0] == "ROLE_ADMIN"){
            /**
             * @var Task[]
             */
            $tasks = $search->searchTaskAdmin($query);
        }else{
            /**
             * @var Task[]
             */
            $tasks = $search->searchTask($query, $loggedInManager->getCompany()->getId());
        }


        if (empty($tasks)) {
            return $this->render('task/not_found.html.twig');
        }

        return $this->render('task/list.html.twig', [
            'status' => "All",
            't' => $query,
            'tasks' => $tasks,
            'username' => $loggedInManager->getUsername(),
        ]);
    }

    #[Route("/filter", name:"filter_task")]
    public function filterTask(Request $request, Filter $filter): Response
    {
        $query = $request->query->get('status');
        $loggedInManager = $this->getUser();
        if($query == "All"){
            return $this->redirectToRoute('all_tasks', ['status' => "All"], Response::HTTP_SEE_OTHER);
        }else{
            if($loggedInManager->getRoles()[0] == "ROLE_ADMIN"){
                $tasks = $filter->filterTaskAdmin($query);
            }else{
                /**
                 * @var Task[]
                 */
                $tasks = $filter->filterTask($query, $loggedInManager->getCompany()->getId());
            }
        }

        if (empty($tasks)) {
            return $this->render('task/not_found.html.twig');
        }
        return $this->render('task/list.html.twig', [
            'status' => $query,
            'tasks' => $tasks,
            'username' => $loggedInManager->getUsername(),
        ]);
    }

    #[Route('/new', name: 'create_task', methods: ['GET', 'POST'])]
    #[IsGranted("ROLE_MANAGER")]
    public function create(Request $request, EntityManagerInterface $entityManager): Response
    {
        $task = new Task();
        $form = $this->createForm(TaskType::class, $task);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $task->setStatus(false);
            $loggedInManager = $this->getUser();
            if ($loggedInManager) {
                $task->setCompany($loggedInManager->getCompany());
            }
//            $assignedToId = $form->get('assignedTo')->getData();
//
//            // Find the corresponding User entity
//            $userRepository = $entityManager->getRepository(User::class);
//            $userObject = $userRepository->find($assignedToId);
//
//            // Set the User object in the Task entity
//            $task->setAssignedTo($userObject);
            $entityManager->persist($task);
            $entityManager->flush();

            return $this->redirectToRoute('all_tasks', ['status' => "All"], Response::HTTP_SEE_OTHER);
        }

        return $this->renderForm('task/new.html.twig', [
            'task' => $task,
            'form' => $form,
        ]);
    }

    #[Route('/{id}', name: 'app_task_show', methods: ['GET'])]
    public function show(Task $task): Response
    {
        $permit = false;
        $loggedInManager = $this->getUser();
        if($loggedInManager->getRoles()[0] == "ROLE_MANAGER" || $loggedInManager->getRoles()[0] == "ROLE_ADMIN"){
            $permit = true;
        }
        return $this->render('task/show.html.twig', [
            'task' => $task,
            'permit' => $permit,
        ]);
    }

    #[Route('/{id}/edit', name: 'app_task_edit', methods: ['GET', 'POST'])]
    #[IsGranted("ROLE_MANAGER")]
    public function edit(Request $request, Task $task, EntityManagerInterface $entityManager): Response
    {
        $form = $this->createForm(TaskType::class, $task);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager->flush();

            return $this->redirectToRoute('all_tasks', ['status' => "All"], Response::HTTP_SEE_OTHER);
        }

        return $this->renderForm('task/edit.html.twig', [
            'task' => $task,
            'form' => $form,
        ]);
    }

    #[Route('/{id}', name: 'app_task_delete', methods: ['POST'])]
    public function delete(Request $request, Task $task, EntityManagerInterface $entityManager): Response
    {
        if ($this->isCsrfTokenValid('delete'.$task->getId(), $request->request->get('_token'))) {
            $entityManager->remove($task);
            $entityManager->flush();
        }

        return $this->redirectToRoute('all_tasks', ['status' => "All"], Response::HTTP_SEE_OTHER);
    }
}
