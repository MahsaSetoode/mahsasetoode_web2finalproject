<?php

namespace App\Form;

use App\Entity\Task;
use App\Entity\User;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\EntityRepository;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Security\Core\Security;

class TaskType extends AbstractType
{

    private $entityManager;
    private $security;

    public function __construct(EntityManagerInterface $entityManager, Security $security)
    {
        $this->entityManager = $entityManager;
        $this->security = $security;
    }

    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $user = $this->security->getUser();
        $builder
            ->add('Title')
            ->add('description')
            ->add('dueDate', DateType::class, [
                'widget' => 'single_text',
                // adds a class that can be selected in JavaScript
                'attr' => ['class' => 'js-datepicker'],
            ])
            ->add('assignedTo',  EntityType::class, [
                'class' => User::class,
                'choice_label' => 'username', // Replace with the property you want to display
                'query_builder' => function(EntityRepository $er) use ($user) {
                    return $er->createQueryBuilder('u')
                        ->where('u.company = :company')
                        ->setParameter('company', $user->getCompany());
                },
                'label' => 'Assign To',
            ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => Task::class,
        ]);
    }

    private function getAssignableUsers($companyId)
    {
        $userRepository = $this->entityManager->getRepository(User::class);
        $users = $userRepository->findBy(['company' => $companyId]);

        $choices = [];
        foreach ($users as $user) {
            $choices[$user->getUsername()] = $user->getId();
        }

        return $choices;
    }
}
