<?php

namespace App\Model;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

trait TimeLoggerTrait
{
    #[ORM\Column(type: 'datetime_immutable')]
    protected \DateTimeImmutable $createdAt;

    #[ORM\Column(type: 'datetime_immutable')]
    protected \DateTimeImmutable $updatedAt;

    public function getCreateAt(): \DateTimeImmutable{
        return $this->createdAt;
    }
    public function setCreateAt(\DateTimeImmutable $createdAt): self{
        $this->createdAt = $createdAt;
        return $this;
    }
    public function getUpdateAt(): \DateTimeImmutable
    {
        return $this->updatedAt;
    }
    public function setUpdateAt(\DateTimeImmutable $updatedAt): self{
        $this->updatedAt = $updatedAt;
        return $this;
    }

}