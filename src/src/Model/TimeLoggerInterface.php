<?php

namespace App\Model;

interface TimeLoggerInterface
{
    public function getCreateAt(): ?\DateTimeImmutable;
    public function setCreateAt(\DateTimeImmutable $createdAt): self;
    public function getUpdateAt(): ?\DateTimeImmutable;
    public function setUpdateAt(\DateTimeImmutable $updatedAt): self;

}