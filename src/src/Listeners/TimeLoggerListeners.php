<?php

namespace App\Listeners;
//use Doctrine\Common\Persistence\Event\LifecycleEventArgs;
use App\Model\TimeLoggerInterface;
use Doctrine\ORM\Event\PreUpdateEventArgs;
use Doctrine\Persistence\Event\LifecycleEventArgs;


class TimeLoggerListeners {
    public function prePersist(LifecycleEventArgs $args)
    {
        $entity = $args->getObject();

        // only act on some "Product" entity
        if (!$entity instanceof TimeLoggerInterface) {
            return;
        }
        $entity->setCreateAt(new \DateTimeImmutable());
        $entity->setUpdateAt(new \DateTimeImmutable());
    }

    public function preUpdate(PreUpdateEventArgs $args)
    {
        $entity = $args->getObject();

        // only act on some "Product" entity
        if (!$entity instanceof TimeLoggerInterface) {
            return;
        }
        $entity->setUpdateAt(new \DateTimeImmutable());
    }
}