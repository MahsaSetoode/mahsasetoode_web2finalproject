<?php

namespace App\Menu;
//use App\Entity\Hotel;
use Doctrine\ORM\EntityManagerInterface;
use Knp\Menu\FactoryInterface;
use Knp\Menu\ItemInterface;
use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Component\Security\Core\Authorization\AuthorizationCheckerInterface;



class Builder{

//    use ContainerAwareTrait;

    private FactoryInterface $factory;
    private EntityManagerInterface $entityManager;
    private AuthorizationCheckerInterface $authorizationChecker;

    public function __construct(EntityManagerInterface $entityManager, FactoryInterface $factory, AuthorizationCheckerInterface $authorizationChecker)
    {
        $this->factory = $factory;
        $this->entityManager = $entityManager;
        $this->authorizationChecker = $authorizationChecker;
    }

    public function mainMenu(RequestStack $requestStack): ItemInterface
    {
        $menu = $this->factory->createItem('root');

        $menu->addChild('Home', ['route' => 'app_home']);


        if ($this->authorizationChecker->isGranted('ROLE_USER')) {
            $menu->addChild('Register your company', ['route' => 'create_company']);
            $menu->addChild('logout', ['route' => 'app_logout']);
        }

        if ($this->authorizationChecker->isGranted('ROLE_MANAGER')) {
            $menu->addChild('Create Task', ['route' => 'create_task']);
            $menu->addChild('Create Employee', ['route' => 'create_user']);
            $menu->addChild('All Tasks', ['route' => 'all_tasks']);
            $menu->addChild('My Tasks', ['route' => 'my_tasks']);
            $menu->addChild('Profile', ['route' => 'user_profile']);
            $menu->addChild('logout', ['route' => 'app_logout']);
        }
        if ($this->authorizationChecker->isGranted('ROLE_ADMIN')) {
            $menu->addChild('All Tasks', ['route' => 'all_tasks']);
            $menu->addChild('logout', ['route' => 'app_logout']);
        }

        if ($this->authorizationChecker->isGranted('ROLE_Employee')) {
            $menu->addChild('All Tasks', ['route' => 'all_tasks']);
            $menu->addChild('My Tasks', ['route' => 'my_tasks']);
            $menu->addChild('Profile', ['route' => 'user_profile']);
            $menu->addChild('logout', ['route' => 'app_logout']);
        }

        if (!$this->authorizationChecker->isGranted('ROLE_Employee')
        && !$this->authorizationChecker->isGranted('ROLE_MANAGER') && !$this->authorizationChecker->isGranted('ROLE_USER')
        && !$this->authorizationChecker->isGranted('ROLE_ADMIN')) {
            $menu->addChild('Login', ['route' => 'app_login']);
            $menu->addChild('Signup', ['route' => 'app_register']);
        }

        $menu->addChild('About us', ['route' => 'app_about']);

        return $menu;
    }

}
