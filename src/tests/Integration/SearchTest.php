<?php

namespace App\Tests\Integration;

use App\Entity\Task;
use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;
use App\Entity\Hotel;
use App\Services\Search;
use App\Repository\HotelRepository;
use Doctrine\ORM\EntityManagerInterface;

class SearchTest extends KernelTestCase
{
    // to create kernel class
    protected static function getKernelClass(): string
    {
        return \App\Kernel::class;
    }

    public function testIntegrationSearch(): void
    {
        $kernel = self::bootKernel();

        $this->assertSame('test', $kernel->getEnvironment());
        /** @var EntityManagerInterface $entityManager */
        $entityManager = static::getContainer()->get(EntityManagerInterface::class);
        $taskRepo = $entityManager->getRepository(Task::class);


        $task1 = new Task();
        $task1->setTitle("abcd");

        $task2 = new Task();
        $task2->setTitle("cdef");


        $taskRepo->save($task1);
        $taskRepo->save($task2, true);

        $searchService = static::getContainer()->get(Search::class);
        $this->assertInstanceOf(Search::class, $searchService);

        $result = $searchService->search("ab");
        $this->assertContains($task1, $result);
        $this->assertNotContains($task2, $result);

        $result = $searchService->search("ef");
        $this->assertContains($task1, $result);
        $this->assertNotContains($task2, $result);

        $result = $searchService->search("cd");
        $this->assertContains($task1, $result);
        $this->assertContains($task2, $result);
        // $routerService = static::getContainer()->get('router');
        // $myCustomService = static::getContainer()->get(CustomService::class);
    }
}
