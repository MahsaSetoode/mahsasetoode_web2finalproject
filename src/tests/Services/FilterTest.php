<?php

namespace App\Tests\Services;

use App\Entity\Task;
use App\Repository\TaskRepository;
use App\Services\Filter;
use Doctrine\ORM\EntityManagerInterface;
use PHPUnit\Framework\TestCase;

class FilterTest extends TestCase
{

    public function testFilterTask()
    {

        $task = new Task();
        $task->setStatus(true); // create expected result

        // Create a mock TaskRepository
        $taskRepository = $this->createMock(TaskRepository::class);
//        $taskRepository->findBy(['status' => true])->willReturn([$task]);
        $taskRepository->expects($this->once())
            ->method('findBy')
            ->with(['status' => true, 'company' => 1])
            ->willReturn([$task]);

        // Create a mock EntityManagerInterface
        $entityManager = $this->createMock(EntityManagerInterface::class);
//        $entityManager->getRepository(Task::class)->willReturn($taskRepository->reveal());
        $entityManager->expects($this->once())
            ->method('getRepository')
            ->with(Task::class)
            ->willReturn($taskRepository);

        $taskService = new Filter($entityManager);

        // actual
        $result = $taskService->filterTask(true, 1);

        $this->assertEquals([$task], $result);
    }
}
